from discord.ext import commands
import discord
from cogs.utils import checks
import requests
from .utils.dataIO import fileIO
import urllib.parse
import os
from __main__ import send_cmd_help


default_whitelist = {}
default_setings = {}


class Gifify:

    def __init__(self, bot):
        self.bot = bot
        self.whitelist = fileIO("data/gifify/whitelist.json", "load")
        self.settings = fileIO("data/gifify/data.json", "load")

    async def get(self, word):
        url = self.settings["settings"]["host"] + urllib.parse.urlencode(
            {"api_key": self.settings["settings"]["api"], "q": word})
        json_data = requests.get(url).json()
        result = json_data["data"][0]["bitly_gif_url"]
        return result

    @commands.group(name="gifify", pass_context=True, no_pm=True, invoke_without_command=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def gifify(self, ctx):
        """
        Controls the functions of this cog
        """
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)

    @checks.mod_or_permissions(manage_channels=True)
    @gifify.command(name="whitelist", pass_context=True, no_pm=True)
    async def _whitelist(self, ctx, channel: discord.Channel=None):
        """
        Adds the mentioned (or current) channel to the whitelist if it doesnt exist, removes if it does exist.
        """
        if channel is None:
            channel = ctx.message.channel

        new_channel = self.bot.get_channel(channel.id)

        server = ctx.message.server.id

        try:
            if self.whitelist[server] is server:
                pass
        except:

            self.whitelist[server] = default_whitelist
            self.whitelist[server]["dummyEntry"] = "Whitelist"
            msg = "Building List"
            fileIO("data/gifify/whitelist.json", "save", self.whitelist)
            await self.bot.send_message(ctx.message.channel, msg)

        if new_channel.id in self.whitelist[server]:
            msg = "The {0} channel is already added to the whitelist".format(new_channel.mention)
            msg += "\n\n\nremoving"
            del self.whitelist[server][new_channel.id]
            fileIO("data/gifify/whitelist.json", "save", self.whitelist)
            await self.bot.send_message(ctx.message.channel, msg)

        elif new_channel.id not in self.whitelist[server]:
            #self.whitelist[server] = default_whitelist
            self.whitelist[server][new_channel.id] = "Whitelisted"
            fileIO("data/gifify/whitelist.json", "save", self.whitelist)
            msg = "The {0} channel is added to the whitelist".format(new_channel.mention)
            await self.bot.send_message(ctx.message.channel, msg)

        else:
            msg = "What is my purpose?"
            await self.bot.send_message(ctx.message.channel, msg)

    @gifify.command(name="api", pass_context=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def api_add(self,ctx, api_key):
        try:
            url = "http://api.giphy.com/v1/gifs/search?" + urllib.parse.urlencode(
                {"api_key": api_key, "q": "sucsess"})
            json_data = requests.get(url).json()
            result = "Success\n" + json_data["data"][0]["bitly_gif_url"]
            self.settings = {"settings": {"host": "http://api.giphy.com/v1/gifs/search?"}}
            self.settings["settings"]["api"] = api_key
            fileIO("data/gifify/data.json", "save", self.settings)
            await self.bot.send_message(ctx.message.channel, result)
        except:
            msg = "Test with apikey failed."
            await self.bot.send_message(ctx.message.channel, msg)

    @gifify.command(name="override", pass_context=True)
    @checks.mod_or_permissions(manage_channels=True)
    async def override(self,ctx, trigger, link):
        try:
            if self.settings["override"]:
                pass
        except:

            self.settings["override"] = {}
            self.settings["override"]["dummyEntry"] = "nais"
            msg = "Building List"
            fileIO("data/gifify/whitelist.json", "save", self.whitelist)
            await self.bot.send_message(ctx.message.channel, msg)


        if trigger in self.settings["override"]:
            msg = "The {0} trigger is already added to the override".format(trigger)
            msg += "\n\n\nremoving"
            del self.settings["override"][trigger]
            fileIO("data/gifify/data.json", "save", self.settings)
            await self.bot.send_message(ctx.message.channel, msg)
        elif trigger not in self.settings["override"]:
            self.settings["override"][trigger] = link
            fileIO("data/gifify/data.json", "save", self.settings)
            msg = "The {0} trigger is added to the override".format(trigger)
            await self.bot.send_message(ctx.message.channel, msg)
        else:
            msg = "I broke"
            await self.bot.send_message(ctx.message.channel, msg)


    ###Pepe8
    async def sniffer(self, message):
        search = str(message.content[:-4])
        if message.author == self.bot.user:
            return
        if message.content.startswith("http://") or message.content.startswith("https://"):
            return
        try:
            if message.channel.id in self.whitelist[message.server.id]:
                if message.content.endswith(".gif"):
                    print("loop")
                    try:
                        print("try")
                        if search in self.settings["override"]:
                            print("tryYes")
                            await self.bot.send_message(message.channel, self.settings["override"][search])
                        else:
                            raise Exception("Not override")
                    except Exception as e:
                        print(e)
                        gif = await Gifify.get(self, search)
                        await self.bot.send_message(message.channel, gif)
                    except:
                        print("Wat")
        except:
            pass


def check_folders():
    if not os.path.exists("data/gifify"):
        print("Creating data/gifify folder...")
        os.makedirs("data/gifify")


def check_files_whitelist():
    f = "data/gifify/whitelist.json"
    if not fileIO(f, "check"):
        print("Creating gifify whitelist.json...")
        fileIO(f, "save", default_whitelist)
    else: #consistency check
        current = fileIO(f, "load")
        for k,v in current.items():
            if v.keys() != {}:
                for key in default_whitelist.keys():
                    if key not in v.keys():
                        current[k][key] = default_whitelist[key]
                        print("Adding " + str(key) + " field to gifify whitelist.json")
        fileIO(f, "save", current)


def check_files_data():
    f = "data/gifify/data.json"
    if not fileIO(f, "check"):
        print("Creating gifify data.json...")
        fileIO(f, "save", default_setings)
    else: #consistency check
        current = fileIO(f, "load")
        for k,v in current.items():
            if v.keys() != {}:
                for key in default_setings.keys():
                    if key not in v.keys():
                        current[k][key] = default_setings[key]
                        print("Adding " + str(key) + " field to gifify data.json")
        fileIO(f, "save", current)


def setup(bot):
    check_folders()
    check_files_data()
    check_files_whitelist()
    n = Gifify(bot)
    bot.add_listener(n.sniffer, "on_message")
    bot.add_cog(n)
